// Ednacsv reads an eDNA Sampler (SADIE) deployment archive file and outputs
// the data as a set of four CSV files.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"

	"bitbucket.org/uwaploe/ednadata"
)

const Usage = `Usage: ednacsv [options] archivefile

Extract the eDNA Sampler (SADIE) deployment data as a set of CSV files.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	outDir string
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&outDir, "dir", outDir, "Output data directory")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		fmt.Fprintln(os.Stderr, "Missing deployment file")
		flag.Usage()
		os.Exit(1)
	}

	d, err := ednadata.ReadDeployment(args[0])
	if err != nil {
		log.Fatalf("Cannot open deployment archive: %v", err)
	}

	err = d.Csv(outDir)
	if err != nil {
		log.Fatal(err)
	}

	if outDir == "" {
		outDir, _ = os.Getwd()
	}

	log.Printf("CSV files created in %q", outDir)
}
