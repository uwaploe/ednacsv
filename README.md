# Extract eDNA Sampler data to CSV files

The eDNA Sampler stores data collected during a deployment to a compressed TAR archive. This program extracts the data to a set of four CSV files (one for each data record type) and one text file. All of the files use linefeeds only to terminate each line (Unix/Linux convention).

- sample_ID.csv
- result_ID.csv
- depth_ID.csv
- battery_ID.csv
- metadata_ID.txt

Where ID is replaced with the deployment ID. The contents of each data record is described in detail on the [eDNA Wiki](https://github.com/sarahewebster/eDNA/wiki).

The metadata file will be empty if there is no *metadata* record in the Sampler output, otherwise it will contain one entry per line. Each entry has the form *key*=*value*.

## Installation

Download the binary for your OS from the Downloads section of this repository, unpack it and install the executable file somewhere in your path.

## Usage

The program accepts a single command line argument:

``` shellsession
$ ednacsv --help
Usage: ednacsv [options] archivefile

Extract the eDNA Sampler (SADIE) deployment data as a set of CSV files.

  -dir string
    	Output data directory
  -version
    	Show program version information and exit
```

The CSV files will be written to the current directory by default, you can write them to a different directory by using the `-dir` command-line option; e.g. `ednacsv -dir=some/other/directory infile`. The shell session below illustrates the procedure on a Linux/Unix system:

``` shellsession
$ mkdir -p benchtest-1
$ ednacsv --dir benchtest-1 ./edna_20210617T221543.tar.gz
2021/06/17 15:47:15 CSV files created in "benchtest-1"
$ ls -l benchtest-1/
total 632
-rw-r--r-- 1 mike     33 2021-06-17 15:47 battery_20210617T221543.csv
-rw-r--r-- 1 mike    128 2021-06-17 15:47 depth_20210617T221543.csv
-rw-r--r-- 1 mike     21 2021-06-17 15:47 metadata_20210617T221543.txt
-rw-r--r-- 1 mike    259 2021-06-17 15:47 result_20210617T221543.csv
-rw-r--r-- 1 mike 627004 2021-06-17 15:47 sample_20210617T221543.csv
```
